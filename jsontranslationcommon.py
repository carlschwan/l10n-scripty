# -*- coding: utf-8 -*-
"""
Copyright 2016 Alex Richardson <arichardson.kde@gmail.com>

Permission to use, copy, modify, and distribute this software
and its documentation for any purpose and without fee is hereby
granted, provided that the above copyright notice appear in all
copies and that both that the copyright notice and this
permission notice and warranty disclaimer appear in supporting
documentation, and that the name of the author not be used in
advertising or publicity pertaining to distribution of the
software without specific, written prior permission.

The author disclaim all warranties with regard to this
software, including all implied warranties of merchantability
and fitness.  In no event shall the author be liable for any
special, indirect or consequential damages or any damages
whatsoever resulting from loss of use, data or profits, whether
in an action of contract, negligence or other tortious action,
arising out of or in connection with the use or performance of
this software.
"""
import csv
import StringIO
import os
import sys
import codecs

debugoutput = os.getenv("VERBOSE")

# see kcoreaddons/kpluginmetadata.cpp for list of translatable fields
translationfields = ['Name', 'Description', 'Copyright', 'ExtraInformation']
kaboutpersonfields = ['Authors', 'Translators', 'OtherContributors']
kioprotocolsfield = 'KDE-KIO-Protocols'
kpluginfield = 'KPlugin'

def serializeList(l):
  # csv is not unicode safe in py2 -> add encode + decode calls
  s = StringIO.StringIO()
  writer = csv.writer(s, delimiter=';', quoting=csv.QUOTE_NONE, escapechar='\\', lineterminator='\n')
  writer.writerow(list(s.encode('utf-8') for s in l))
  return unicode(s.getvalue()[:-1], "utf-8")  # remove final newline and convert to unicode


def deserializeList(s):
  # use csv to parse a ; delimited list with escape char
  # have to encode it as utf-8 otherwise this call will fail
  reader = csv.reader(StringIO.StringIO(s.encode("utf-8")), delimiter=';', quoting=csv.QUOTE_NONE, escapechar='\\')
  result = reader.next()
  return [unicode(s, encoding="utf-8") for s in result]


def debugPrint(msg):
  global debugoutput
  if debugoutput:
    sys.stderr.write(msg + '\n')
