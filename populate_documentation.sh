#!/bin/bash
# Author: Albert Astals Cid <aacid@kde.org>
# License: WTFPL 2 - http://sam.zoy.org/wtfpl/

function git_download
{
	# git_download ${repo_url} "${module_name}" "${module_branch}" "${doc_dir}" "$silentflag" $oldmd5
	if [ -z "$5" ]; then
		oldmd5=$6
		newmd5=`mktemp`
	fi
        # compute the tarball name (fix the branch replacing / with -)
	local tarfilename="${2}-$(echo ${3} | tr '/' '-').tar.gz"
	local doc_dir_arg=""
	local extract_levels=1
	if [ -n "${doc_dir}" ]; then
		doc_dir_arg="?path=${doc_dir}"
		# find out how many directories should be removed when
		# extracting the tarball to get the documentation files
		let extract_levels+=$(echo "${doc_dir}" | awk -F/ '{ print NF }')
	fi
	timeout 1h curl -s https://invent.kde.org/${1}/-/archive/${3}/${tarfilename}${doc_dir_arg} | tar -xz --strip-components=${extract_levels}
	res=$?
	if [ $res -ne 0 ]; then
		echo "Failed to download $1 $3:$4"
	fi
	if [ -z "$5" ]; then
		hashdeep -rl -j0 . > $newmd5
		if [ -n "$oldmd5" ]; then
			differences=`diff -ub $oldmd5 $newmd5 | grep ^[+-] | grep -v ^+++ | grep -v ^--- | cut -d " " -f 3 | uniq`
		else
			differences=`ls`
		fi
		if [ -z "$differences" ]; then
			echo "No differences"
		else
			echo "New/changed/removed files:"
			echo $differences
		fi
		rm -f $oldmd5 $newmd5
	fi
	return $res
}

dir=`dirname $0`
l10ndir="$(readlink -f $dir)"

if [ ! -f ${l10ndir}/get_paths ] || [ ! -f ${l10ndir}/documentation_paths ]; then
	echo "You have to run the script from the directory which contains get_paths and documentation_paths"
	exit
fi

. $l10ndir/get_paths
check_kde_projects_requirements

mkdir -p documentation
cd documentation

onlyonemodule=""
sawentry=0
lastupdate=0

silentflag=""
if test "$1" = "--silent"; then
	silentflag="-q"
	shift
fi

if [ -n "$1" ]; then
	onlyonemodule=$1
fi

while read line; do
	if [ -z "$line" ]; then
		continue
	fi

	if [[ $line = \#* ]]; then
		continue
	fi
		
	IFS=' ' read -ra tokens <<< $line
	ntokens=${#tokens[@]}
	if [ $ntokens -lt 2 ]; then
		echo "The number of tokens is not valid - $line"
	else
		if [ "${tokens[0]}" = "entry" ]; then
			if [ $ntokens -lt 3 ] || [ $ntokens -gt 4 ]; then
				echo "The number of tokens (${ntokens}) is not valid - ${line}"
			else
				doc_dir=""
				if [ $ntokens -eq 4 ]; then
					doc_dir="${tokens[3]}"
				fi
				entry_dir="${tokens[1]}"

				module_name="${tokens[2]}"
				if [ -n "${onlyonemodule}" ] && [ "${onlyonemodule}" != "${module_name}" ]; then
					# if a specific module is requested, only update it
					continue
				fi

				module_branch="$(get_branch ${module_name})"

				if [ "${module_branch}" = "get_branch_none" ]; then
					echo "Unknown branch for ${module_name}/${entry_dir}"
				else
					if [ -z "$silentflag" ]; then
						echo "Updating ${module_name}/${entry_dir} documentation (${module_branch})"
					fi
					savepwd=$PWD
					mkdir -p ${module_name}
					cd ${module_name}
					oldmd5=""
					if [ -d ${entry_dir} ]; then
						if [ -z "$silentflag" ]; then
							oldmd5=`mktemp`
							(
							cd ${entry_dir}
							hashdeep -rl -j0 . > $oldmd5
							)
						fi
						rm -rf ${entry_dir}
					fi
					mkdir -p ${entry_dir}
					cd ${entry_dir}
					repo_url="$(get_full_repo_path ${module_name})"
					git_download ${repo_url} "${module_name}" "${module_branch}" "${doc_dir}" "$silentflag" $oldmd5
					lastupdate=$?
					cd $savepwd
					sawentry=1
				fi
			fi
		else
			echo "Invalid line - $line"
		fi
	fi
	if [ "x$lastupdate" != "x0" ]; then
		echo "There was an error updating the sources"
		exit 1
	fi
done < "${l10ndir}/documentation_paths"

cd ..

